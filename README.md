# shenv

The `libshenv` library, `shenv` command-line tool and `shenv` Python modules
allow a user to create an environment and add, update or remove variables that
are shared between multiple processes. The environment is stored in a shared
memory object and protected by a file lock to prevent race conditions.

The size of the shared memory object is fixed at 64 KiB and supports 256
environment strings of at most 255 characters, excluding the terminating null
byte.

The `libshenv` API and `shenv` tool mimick the Glibc API for regular environment
variables and the GNU `env` tool, respectively. The `shenv` Python module
provides a `SharedEnvironment` class which implements the
[MutableMapping](https://docs.python.org/3/library/collections.abc.html#collections.abc.MutableMapping)
interface.

## Installation

shenv uses the Autotools build system. To build, run:

    $ autoreconf -i
    $ ./configure
    $ make

To install, run:

    $ make install

The Python mopdule uses PyPA's
[build](https://packaging.python.org/en/latest/key_projects/#build). On
Unix/macOS, this package builder can be installed with:

    $ python3 -m pip install --upgrade build

To build the module, run:

    $ python3 -m build

This creates a `.whl` and `.tar.gz` file in the `dist` directory, which can be
installed with pip or uploaded with twine.

## Documentation

See the [shenv](doc/shenv.md.in) man page for documentation.

## Licensing

Copyright 2022 [Lely Industries N.V.](http://www.lely.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
