"""A module to share environment variables between processes"""

import collections.abc
import subprocess


class SharedEnvironment(collections.abc.MutableMapping):
    """Provides a MutableMapping interface to shared environment variables"""

    def __init__(self, name: str):
        self._name = name

    def __setitem__(self, k, v) -> None:
        subprocess.run(
            ["shenv", self._name, "{}={}".format(k, v)],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

    def __getitem__(self, k):
        for s in self.strings():
            name, val = s.split("=")
            if k == name:
                return val
        raise KeyError(k)

    def __delitem__(self, k) -> None:
        subprocess.run(
            ["shenv", "--unset={}".format(k), self._name],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

    def __iter__(self):
        for s in self.strings():
            yield s.split("=")

    def __len__(self):
        return len(self.strings())

    def clear(self):
        subprocess.run(["shenv", "-r", self._name])

    def strings(self):
        """Return a list of environment strings"""
        stdout = subprocess.getoutput("shenv {}".format(self._name))
        return stdout.splitlines()
