// SPDX-License-Identifier: Apache-2.0
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <shenv.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CMD "shenv"

#define HELP \
	"Usage: " CMD " [OPTION]... [-] SHM_NAME [NAME=VALUE]... \n" \
	"Set each NAME to VALUE in the shared environment stored in the shared memory\n" \
	"object given by SHM_NAME. The resulting environment is printed to the standard\n" \
	"output.\n\n" \
	"Options:\n" \
	"  -0, --null        end each output line with NUL instead of newline\n" \
	"  -c, --clear       clear the existing environment\n" \
	"  -h, --help        show this information\n" \
	"  -r, --remove      remove the shared memory object\n" \
	"  -u, --unset=NAME  remove a variable from the environment\n"

#define FLAG_ERROR 0x1
#define FLAG_NULL 0x2
#define FLAG_CLEAR 0x4
#define FLAG_REMOVE 0x8

int
main(int argc, char *argv[])
{
	int flags = 0;
	int uc = 0;
	char *uv[SHENV_NUM + 1] = { NULL };

	opterr = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-')
			break;
		if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "clear")) {
				flags |= FLAG_CLEAR;
			} else if (!strcmp(arg, "help")) {
				puts(HELP);
				return EXIT_SUCCESS;
			} else if (!strcmp(arg, "null")) {
				flags |= FLAG_NULL;
			} else if (!strcmp(arg, "remove")) {
				flags |= FLAG_REMOVE;
			} else if (!strncmp(arg, "unset=", 6)) {
				if (uc < SHENV_NUM) {
					uv[uc++] = arg + 6;
				} else {
					fprintf(stderr,
							CMD
							": too many variables specified -- %s\n",
							arg + 6);
					flags |= FLAG_ERROR;
				}
			} else {
				fprintf(stderr, CMD ": illegal option -- %s\n",
						arg);
				flags |= FLAG_ERROR;
			}
		} else {
			int c = getopt(argc, argv, "0hcru:");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr,
						CMD
						": option requires an argument -- %c\n",
						optopt);
				flags |= FLAG_ERROR;
				break;
			case '?':
				fprintf(stderr, CMD ": illegal option -- %c\n",
						optopt);
				flags |= FLAG_ERROR;
				break;
			case '0': flags |= FLAG_NULL; break;
			case 'c': flags |= FLAG_CLEAR; break;
			case 'h': puts(HELP); return EXIT_SUCCESS;
			case 'r': flags |= FLAG_REMOVE; break;
			case 'u':
				if (uc < SHENV_NUM) {
					uv[uc++] = optarg;
				} else {
					fprintf(stderr,
							CMD
							": too many variables specified -- %s\n",
							optarg);
					flags |= FLAG_ERROR;
				}
				break;
			}
		}
	}

	if (optind >= argc) {
		fprintf(stderr, CMD ": no shared memory object specified\n");
		flags |= FLAG_ERROR;
	}

	if (flags & FLAG_ERROR) {
		fputs(HELP, stderr);
		return EXIT_FAILURE;
	}

	const char *shm_name = argv[optind++];
	shenv_t *shenv = shenv_open(shm_name);
	if (!shenv) {
		perror(shm_name);
		return EXIT_FAILURE;
	}

	if (flags & FLAG_CLEAR) {
		shenv_clear(shenv);
	} else {
		for (int i = 0; i < uc; i++)
			shenv_unset(shenv, uv[i]);
	}

	for (; optind < argc; optind++)
		shenv_put(shenv, argv[optind]);

	if (!(flags & FLAG_REMOVE))
		shenv_print((flags & FLAG_NULL) ? 0 : '\n', shenv);

	shenv_close(shenv);

	if (flags & FLAG_REMOVE)
		shenv_unlink(shm_name);

	return EXIT_SUCCESS;
}
