// SPDX-License-Identifier: Apache-2.0
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <shenv.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define SHENV_LEN (SHENV_NUM * (SHENV_MAX + 1))

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

struct shenv {
	int fd;
	void *addr;
};

static int shenv_lock(const shenv_t *shenv);
static int shenv_unlock(const shenv_t *shenv);

static char *shenv_find_l(
		const shenv_t *shenv, const char *name, size_t name_size);

static int shenv_put_(shenv_t *shenv, const char *string, int overwrite);

shenv_t *
shenv_open(const char *name)
{
	int errsv = 0;

	shenv_t *shenv = malloc(sizeof(shenv_t));
	if (!shenv) {
		errsv = errno;
		goto error_malloc_shenv;
	}

	shenv->fd = shm_open(name, O_RDWR | O_CREAT, 0644);
	if (shenv->fd == -1) {
		errsv = errno;
		goto error_open_fd;
	}

	// Lock the semaphore to prevent a race condition with the
	// initialization of the shared memory.
	if (shenv_lock(shenv) == -1) {
		errsv = errno;
		goto error_lock;
	}

	struct stat st;
	if (fstat(shenv->fd, &st) == -1) {
		errsv = errno;
		goto error_fstat;
	}

	if (st.st_size != SHENV_LEN) {
		// Truncate the file to 0 first to remove any previous contents.
		if (st.st_size > 0 && ftruncate(shenv->fd, 0) == -1) {
			errsv = errno;
			goto error_ftuncate;
		}
		if (ftruncate(shenv->fd, SHENV_LEN) == -1) {
			errsv = errno;
			goto error_ftuncate;
		}
	}

	shenv->addr = mmap(NULL, SHENV_LEN, PROT_READ | PROT_WRITE, MAP_SHARED,
			shenv->fd, 0);
	if (shenv->addr == MAP_FAILED) {
		errsv = errno;
		goto error_mmap;
	}

	shenv_unlock(shenv);

	return shenv;

	// munmap(shenv->addr, SHENV_LEN);
error_mmap:
error_ftuncate:
error_fstat:
	shenv_unlock(shenv);
error_lock:
	close(shenv->fd);
error_open_fd:
	free(shenv);
error_malloc_shenv:
	errno = errsv;
	return NULL;
}

int
shenv_close(shenv_t *shenv)
{
	assert(shenv);

	int fd = shenv->fd;
	void *addr = shenv->addr;

	free(shenv);

	return !(munmap(addr, SHENV_LEN) | close(fd)) ? 0 : -1;
}

int
shenv_unlink(const char *name)
{
	return shm_unlink(name);
}

int
shenv_clear(shenv_t *shenv)
{
	assert(shenv);

	if (shenv_lock(shenv) == -1)
		return -1;

	memset(shenv->addr, 0, SHENV_LEN);

	shenv_unlock(shenv);

	return 0;
}

const char *
shenv_get(const shenv_t *shenv, const char *name)
{
	assert(shenv);

	if (!name || !*name || strchr(name, '='))
		return NULL;

	size_t name_size = strlen(name);
	if (name_size > SHENV_MAX - 1)
		return NULL;

	if (shenv_lock(shenv) == -1)
		return NULL;

	char *var = shenv_find_l(shenv, name, name_size);
	if (!var || !*var) {
		shenv_unlock(NULL);
		return NULL;
	}

	// A value string is at least 2 characters shorter than an environment
	// string since it does not contain the name and the `=` character.
	static _Thread_local char val[SHENV_MAX - 1];
	strncpy(val, var + name_size + 1, SHENV_MAX - 2);

	shenv_unlock(shenv);

	return val;
}

int
shenv_put(shenv_t *shenv, const char *string)
{
	assert(shenv);

	// clang-format off
	return !string || strchr(string, '=')
			? shenv_put_(shenv, string, 1)
			: shenv_unset(shenv, string);
	// clang-format on
}

int
shenv_set(shenv_t *shenv, const char *name, const char *val, int overwrite)
{
	assert(shenv);

	if (!name || !*name || strchr(name, '=')) {
		errno = EINVAL;
		return -1;
	}

	size_t name_size = strlen(name);
	size_t val_size = val ? strlen(val) : 0;
	if (name_size > SHENV_MAX - 1 || val_size > SHENV_MAX - 1
			|| name_size + val_size > SHENV_MAX - 1) {
		errno = ENOMEM;
		return -1;
	}

	char string[SHENV_MAX + 1] = { 0 };
	memcpy(string, name, name_size);
	string[name_size] = '=';
	if (val)
		memcpy(string + name_size + 1, val, val_size);

	return shenv_put_(shenv, string, overwrite);
}

int
shenv_unset(shenv_t *shenv, const char *name)
{
	assert(shenv);

	if (!name || !*name || strchr(name, '=')) {
		errno = EINVAL;
		return -1;
	}

	size_t name_size = strlen(name);
	if (name_size > SHENV_MAX - 1)
		return 0;

	if (shenv_lock(shenv) == -1)
		return -1;

	char *var = shenv_find_l(shenv, name, name_size);
	if (var && *var) {
		char *begin = shenv->addr;
		char *end = begin + SHENV_LEN;

		memmove(var, var + SHENV_MAX + 1, end - (var + SHENV_MAX + 1));
		memset(end - (SHENV_MAX + 1), 0, SHENV_MAX + 1);
	}

	shenv_unlock(shenv);

	return 0;
}

int
shenv_fprint(FILE *stream, int delimiter, const shenv_t *shenv)
{
	assert(shenv);

	size_t result = 0;

	if (shenv_lock(shenv) == -1)
		return -1;

	const char *begin = shenv->addr;
	const char *end = begin + SHENV_LEN;
	for (const char *var = begin; var != end && *var;
			var += SHENV_MAX + 1) {
		size_t len = strlen(var);
		if (fputs(var, stream) < 0 || fputc(delimiter, stream) < 0) {
			shenv_unlock(shenv);
			return -1;
		}
		result += len + 1;
	}

	shenv_unlock(shenv);

	return result;
}

int
shenv_print(int delimiter, const shenv_t *shenv)
{
	return shenv_fprint(stdout, delimiter, shenv);
}

int
shenv_snprint(char *s, size_t n, int delimiter, const shenv_t *shenv)
{
	assert(shenv);

	size_t result = 0;

	if (shenv_lock(shenv) == -1)
		return -1;

	const char *begin = shenv->addr;
	const char *end = begin + SHENV_LEN;
	for (const char *var = begin; var != end && *var;
			var += SHENV_MAX + 1) {
		size_t len = strlen(var);
		if (s && result < n) {
			memcpy(s + result, var, MIN(len, n - result - 1));
			if (result + len < n)
				s[result + len] = (unsigned char)delimiter;
		}
		result += len + 1;
	}

	shenv_unlock(shenv);

	if (s && n > 0)
		// Ensure the string is null-terminated.
		s[result < n ? result : n - 1] = 0;

	return result;
}

static int
shenv_lock(const shenv_t *shenv)
{
	assert(shenv);

	int result;
	int errsv = errno;
	do {
		errno = errsv;
		result = lockf(shenv->fd, F_LOCK, 0);
	} while (result == -1 && errno == EINTR);
	return result;
}

static int
shenv_unlock(const shenv_t *shenv)
{
	assert(shenv);

	int result;
	int errsv = errno;
	do {
		errno = errsv;
		result = lockf(shenv->fd, F_ULOCK, 0);
	} while (result == -1 && errno == EINTR);
	return result;
}

static char *
shenv_find_l(const shenv_t *shenv, const char *name, size_t name_size)
{
	assert(shenv);
	assert(name);
	assert(name_size <= SHENV_MAX - 1);

	char *begin = shenv->addr;
	char *end = begin + SHENV_LEN;
	for (char *var = begin; var != end; var += SHENV_MAX + 1) {
		if (!*var)
			return var;
		if (!strncmp(var, name, name_size) && var[name_size] == '=')
			return var;
	}
	return NULL;
}

static int
shenv_put_(shenv_t *shenv, const char *string, int overwrite)
{
	assert(shenv);

	const char *val;
	if (!string || !(val = strchr(string, '=')) || val == string) {
		errno = EINVAL;
		return -1;
	}
	val++;

	if (shenv_lock(shenv) == -1)
		return -1;

	char *var = shenv_find_l(shenv, string, val - string - 1);
	if (var && (!*var || overwrite))
		strncpy(var, string, SHENV_MAX);

	shenv_unlock(shenv);

	return 0;
}
