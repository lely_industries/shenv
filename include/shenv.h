// SPDX-License-Identifier: Apache-2.0
#ifndef SHENV_H_
#define SHENV_H_

#include <stdio.h>

/**
 * The maximum number of bytes in a shared environment string, excluding the
 * terminating null byte.
 */
#define SHENV_MAX 255

/// The maximum number of shared environment strings.
#define SHENV_NUM 256

/// An opaque shared environment type.
typedef struct shenv shenv_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Creates a new, or opens an existing, shared memory object to be used as a
 * shared environment. The environment SHOULD be closed with shenv_close() and
 * can be removed with shenv_unlink().
 *
 * @param name a pointer to the name of the shared environment. For portability,
 *             this SHOULD be a string containing an initial '/', followed by
 *             one or more characters, none of which are '/'.
 *
 * @returns a pointer to a shared environment on success, or NULL on error. In
 * the latter case, the error number is stored in `errno`.
 *
 * @see shenv_close(), shenv_unlink()
 */
shenv_t *shenv_open(const char *name);

/**
 * Closes a shared environment previously opened with shenv_open(). Closing the
 * environment does not remove it and does not affect other processes using the
 * same environment.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_open(), shenv_unlink()
 */
int shenv_close(shenv_t *shenv);

/**
 * Removes a shared environment previously opened with shenv_open(). Removing an
 * environment does not close it and does not affect other processes that have
 * already opened the same environment.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_open(), shenv_close()
 */
int shenv_unlink(const char *name);

/**
 * Clears a shared environment of all name-value pairs.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_get(), shenv_put(), shenv_set(), shenv_unset()
 */
int shenv_clear(shenv_t *shenv);

/**
 * Searches a shared environment for a variable of the specified name and, if it
 * exists, returns a pointer to a (thread-local) copy of its value.
 *
 * The returned pointer remains valid until the next call to shenv_clear(),
 * shenv_get(), shenv_put(), shenv_set() or shenv_unset() from the same thread.
 *
 * @param shenv a pointer to a shared environment previously opened by
 *              shenv_open().
 * @param name  a pointer to the name of the variable.
 *
 * @returns a pointer to the value, or NULL if not found or an error occurred.
 * In the latter case, the error number is stored in `errno`.
 *
 * @see shenv_clear(), shenv_put(), shenv_set(), shenv_unset()
 */
const char *shenv_get(const shenv_t *shenv, const char *name);

/**
 * Adds, updates or removes a variable in a shared environment.
 *
 * This function is analogous to the POSIX `putenv()` function.
 *
 * @param shenv  a pointer to a shared environment previously opened by
 *               shenv_open().
 * @param string a pointer to a string of the form "name=value" if a variable is
 *               to be added or updated, or of the form "name" if a variable is
 *               to be removed.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_clear(), shenv_get(), shenv_set(), shenv_unset()
 */
int shenv_put(shenv_t *shenv, const char *string);

/**
 * Adds or updates a variable in a shared environment.
 *
 * This function is analogous to the POSIX `setenv()` function.
 *
 * @param shenv     a pointer to a shared environment previously opened by
 *                  shenv_open().
 * @param name      a pointer to the name of the variable to be added or
 *                  altered. This name MUST NOT be empty and MUST NOT contain a
 *                  '=' character.
 * @param val       a pointer to the new value of the variable (can be NULL).
 * @param overwrite a flag indicating whether the variable should be updated if
 *                  it already exists.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_clear(), shenv_get(), shenv_put(), shenv_unset()
 */
int shenv_set(shenv_t *shenv, const char *name, const char *val, int overwrite);

/**
 * Removes a variable from a shared environment. It is not an error if the
 * variable does not exist.
 *
 * This function is analogous to the POSIX `unsetenv()` function.
 *
 * @param shenv a pointer to a shared environment previously opened by
 *              shenv_open().
 * @param name  a pointer to the name of the variable to be removed.
 *
 * @returns 0 on success, or -1 on error. In the latter case, the error number
 * is stored in `errno`.
 *
 * @see shenv_clear(), shenv_get(), shenv_put(), shenv_set()
 */
int shenv_unset(shenv_t *shenv, const char *name);

/**
 * Prints the contents of a shared environment to an output stream.
 *
 * @param stream    a pointer to an output stream.
 * @param delimiter the character used as a delimiter between successive
 *                  environment variables.
 * @param shenv     a pointer to a shared environment previously opened by
 *                  shenv_open().
 *
 * @returns the number of characters written, or -1 on error. In the latter
 * case, the error number is stored in `errno`.
 *
 * @see shenv_print(), shenv_snprint()
 */
int shenv_fprint(FILE *stream, int delimiter, const shenv_t *shenv);

/**
 * Prints the contents of a shared environment to the standard output stream.
 *
 * @param delimiter the character used as a delimiter between successive
 *                  environment variables.
 * @param shenv     a pointer to a shared environment previously opened by
 *                  shenv_open().
 *
 * @returns the number of characters written, or -1 on error. In the latter
 * case, the error number is stored in `errno`.
 *
 * @see shenv_fprint(), shenv_snprint()
 */
int shenv_print(int delimiter, const shenv_t *shenv);

/**
 * Prints the contents of a shared environment to a string buffer.
 *
 * @param s         the address of the output buffer. If <b>s</b is ot NULL, at
 *                  most `n - 1` characters are written, plus a terminating null
 *                  byte.
 * @param n         the size (in bytes) of the buffer at <b>s</b>. If <b>n</b>
 *                  is zero, nothing is written.
 * @param delimiter the character used as a delimiter between successive
 *                  environment variables.
 * @param shenv     a pointer to a shared environment previously opened by
 *                  shenv_open().
 *
 * @returns the number of characters that would have been written had the
 * buffer been sufficiently large, not counting the terminating null byte, or -1
 * on error. In the latter case, the error number is stored in `errno`.
 *
 * @see shenv_fprint(), shenv_print()
 */
int shenv_snprint(char *s, size_t n, int delimiter, const shenv_t *shenv);

#ifdef __cplusplus
}
#endif

#endif // SHENV_H_
